var relation = new Array();
var fds = new Array();
var candidateKeys = new Array();
var is3nf;
var isBCNF;
var permResults;
var cover;
var fdsPlusTrans = new Array();
var exportArray = new Array();

function readIn1() {
    var input;
    var output;

    relation = new Array();

    input = document.getElementById("in1").value;
    
    // falls kein komma in eingabe vorhanden
    var x = input.indexOf(",");
    if (x == -1) {
        // falls kein leerzeichen in eingabe vorhanden
        x = input.indexOf(" ");
        if (x == -1) {
            // split nach jedem zeichen
            relationInput = input.split("");
        } else {
            // split nach jedem leerzeichen
            relationInput = input.split(" ");
        }
    } else {
        // falls komma leerzeichen vorhanden
        x = input.indexOf(", ");
        if (x != -1) {
            // split nach jedem komma leerzeichen
            relationInput = input.split(", ");
        } else {
            // split nach jedem komma
            relationInput = input.split(",");
        }
    }

    // bereinige input
    for (var i = 0; i < relationInput.length; i++) {
        relationInput[i] = relationInput[i].trim();
        if (relationInput[i] != "") {
            relation.push(relationInput[i]);
        }
    }

    relation = uniq(relation);

    output = attributePrint(relation);
    
    document.getElementById("out1").innerHTML = output;
}

function readIn2() {
    var input;
    var output;

    is3nf = false;
    isBCNF = false;
    cover = 1;

    document.getElementById('outvon').innerHTML = cover;

    fds = new Array();

    input = document.getElementById("in2").value;

    fdLines = input.split("\n");

    for (var i = 0; i < fdLines.length; i++) {
        if(fdLines[i].trim().length === 0) {
            continue;
        }

        var fd = {left: new Array(), right: new Array()};
        var fdInput = {left: new Array(), right: new Array()};
        var fdLeft = fdLines[i].split('->')[0];
        var fdRight = fdLines[i].split('->')[1];
    
        // falls kein komma in eingabe vorhanden
        var x = fdLines[i].indexOf(",");
        if (x == -1) {
            // falls kein leerzeichen in eingabe vorhanden
            x = fdLeft.indexOf(" ");
            if (x == -1) {
                // falls ein existierendes grosses attribut
                if (relation.indexOf(fdLeft) != -1) {
                    fdInput.left = [fdLeft];
                } else {
                    // split nach jedem zeichen
                    fdInput.left = fdLeft.split("");
                }
            } else {
                // split nach jedem leerzeichen
                fdInput.left = fdLeft.split(" ");
            }
        } else {
            // falls komma leerzeichen vorhanden
            x = fdLeft.indexOf(", ");
            if (x != -1) {
                // split nach jedem komma leerzeichen
                fdInput.left = fdLeft.split(", ");
            } else {
                // split nach jedem komma
                fdInput.left = fdLeft.split(",");
            }
        }

        // falls kein komma in eingabe vorhanden
        x = fdLines[i].indexOf(",");
        if (x == -1) {
            // falls kein leerzeichen in eingabe vorhanden
            x = fdRight.indexOf(" ");
            if (x == -1) {
                // falls ein existierendes grosses attribut
                if (relation.indexOf(fdRight) != -1) {
                    fdInput.right = [fdRight];
                } else {
                    // split nach jedem zeichen
                    fdInput.right = fdRight.split("");
                }
            } else {
                // split nach jedem leerzeichen
                fdInput.right = fdRight.split(" ");
            }
        } else {
            // falls komma leerzeichen vorhanden
            x = fdRight.indexOf(", ");
            if (x != -1) {
                // split nach jedem komma leerzeichen
                fdInput.right = fdRight.split(", ");
            } else {
                // split nach jedem komma
                fdInput.right = fdRight.split(",");
            }
        }

        // bereinige input
        for (var j = 0; j < fdInput.left.length; j++) {
            fdInput.left[j] = fdInput.left[j].trim();
            if (fdInput.left[j] != "") {
                fd.left.push(fdInput.left[j]);
            }
        }

        for (var j = 0; j < fdInput.right.length; j++) {
            fdInput.right[j] = fdInput.right[j].trim();
            if (fdInput.right[j] != "") {
                fd.right.push(fdInput.right[j]);
            }
        }
        
        fds.push(fd);
    }

    exportArray = new Array();

    // fuege zu export hinzu
    exportArray.push(['Attribute']);
    exportArray.push([document.getElementById("in1").value]);
    exportArray.push(['']);
    exportArray.push(['Funktionale Abhängigkeiten']);
    
    var output = '';
    for (var i = 0; i < fds.length; i++) {
        output = output + attributePrint(fds[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(fds[i].right);
        output = output + '<br/>';
        // fuege zu export hinzu
        exportArray.push([fds[i].left + ' -> ' + fds[i].right]);
    }

    // fuege zu export hinzu
    exportArray.push(['']);

    document.getElementById('out2').innerHTML = output;

    calcCandidateKeys();
}

/*****************************************
*          SCHLUESSELBESTIMMUNG          *
*****************************************/

function calcCandidateKeys() {
    // fuege zu export hinzu
    exportArray.push(['Schlüsselbestimmung']);

    var output = '';

    var leftOnly = getLeftOnly();
    var leftAndRight = getLeftAndRight();
    var rightOnly = getRightOnly();
    var noSide = getNoSide(leftOnly, leftAndRight, rightOnly);

    document.getElementById('out3').innerHTML = attributePrint(noSide) + ' <b>müssen</b> daher Teil eines Kandidatenschlüssels sein.';
    // fuege zu export hinzu
    exportArray.push(['Attribute die auf keiner Seite vorkommen']);
    if (noSide.length > 0) {
        exportArray.push([noSide.toString()]);
    } else {
        exportArray.push(['keine']);
    }

    document.getElementById('out4').innerHTML = attributePrint(leftOnly) + ' <b>müssen</b> daher Teil eines Kandidatenschlüssels sein.';
    // fuege zu export hinzu
    exportArray.push(['Attribute die nur links vorkommen']);
    if (leftOnly.length > 0) {
        exportArray.push([leftOnly.toString()]);
    } else {
        exportArray.push(['keine']);
    }

    document.getElementById('out5').innerHTML = attributePrint(leftAndRight) + ' <b>können</b> daher Teil eines Kandidatenschlüssels sein.';
    // fuege zu export hinzu
    exportArray.push(['Attribute die links und rechts vorkommen']);
    if (leftAndRight.length > 0) {
        exportArray.push([leftAndRight.toString()]);
    } else {
        exportArray.push(['keine']);
    }

    document.getElementById('out6').innerHTML = attributePrint(rightOnly) + ' <b>können</b> daher <b>nicht</b>  Teil eines Kandidatenschlüssels sein.';
    // fuege zu export hinzu
    exportArray.push(['Attribute die nur rechts vorkommen']);
    if (rightOnly.length > 0) {
        exportArray.push([rightOnly.toString()]);
    } else {
        exportArray.push(['keine']);
    }

    var possibleAttributes = leftOnly.concat(leftAndRight).concat(noSide);
    possibleAttributes = uniq(possibleAttributes);
    var possibleCombos = getSubArrays(possibleAttributes);

    var possibleCombosWithRequired = Array.from(possibleCombos);

    // entferne moeglichkeiten die nicht alle benoetigten attribute beinhalten
    for (var i = 0; i < possibleCombos.length; i++) {
        for (var j = 0; j < leftOnly.length; j++) {
            if (possibleCombos[i].indexOf(leftOnly[j]) === -1) {
                if (possibleCombosWithRequired.indexOf(possibleCombos[i]) != -1) {
                    possibleCombosWithRequired.splice(possibleCombosWithRequired.indexOf(possibleCombos[i]), 1);
                }
            }
        }
        for (var k = 0; k < noSide.length; k++) {
            if (possibleCombos[i].indexOf(noSide[k]) === -1) {
                if (possibleCombosWithRequired.indexOf(possibleCombos[i]) != -1) {
                    possibleCombosWithRequired.splice(possibleCombosWithRequired.indexOf(possibleCombos[i]), 1);
                }
            }
        }
    }

    possibleCombosWithRequired = uniq(possibleCombosWithRequired);

    output = '';
    var l = 0;
    
    for (var i = 0; i < possibleCombosWithRequired.length; i++) {
        if (possibleCombosWithRequired[i].length > l) {
            l = possibleCombosWithRequired[i].length;
        }
    }

    candidateKeys = new Array();

    for (var i = 1; i < l + 1; i++) {
        output = output + '<h5>Attributhüllen mit |Attributmenge| = ' + i + '</h5><p>';
        // fuege zu export hinzu
        exportArray.push(['Attributhüllen mit |Attributmenge| = ' + i]);
        for (var j = 0; j < possibleCombosWithRequired.length; j++) {
            if (possibleCombosWithRequired[j].length === i) {
                var hull = calcAttrHull(fds, possibleCombosWithRequired[j]);
                var bold = '';
                var boldEnd = '';
                if (!keyContained(candidateKeys, possibleCombosWithRequired[j])) {
                    if (hull.length === relation.length) {
                        candidateKeys.push(possibleCombosWithRequired[j]);
                        bold = '<b>';
                        boldEnd = '</b>';
                    }
                    output = output + bold + 'AttrHülle(F, { '+ prettyPrint(uniq(possibleCombosWithRequired[j])) + ' }) = { ' + prettyPrint(hull) + ' }' + boldEnd + '<br>';
                    // fuege zu export hinzu
                    exportArray.push(['AttrHülle(F, { ' + uniq(possibleCombosWithRequired[j]) + ' }) = { ' + hull + ' }']);
                }
            }
        }
        if (output.endsWith('</h5><p>')) {
            output = output + 'keine möglich oder sinnvoll';
            // fuege zu export hinzu
            exportArray.push(['keine möglich oder sinnvoll']);
        }
        output = output + '</p>';
    }

    document.getElementById('out7').innerHTML = output;

    output = '<h5>Kandidatenschlüssel</h5><p>';
    // fuege zu export hinzu
    exportArray.push(['Kandidatenschlüssel']);
    for (var i = 0; i < candidateKeys.length; i++) {
        output = output + attributePrint(candidateKeys[i]) + '</p>';
        // fuege zu export hinzu
        exportArray.push([candidateKeys[i].toString()]);
    }

    // fuege zu export hinzu
    exportArray.push(['']);

    document.getElementById('out8').innerHTML = output;

    test2nf();

    var permFds = perm(fds);
    permResults = new Array();

    for (var i = 0; i < permFds.length; i++) {
        var result = {fds: new Array(), cov: new Array()};
        result.fds = permFds[i];
        result.cov = canonicalCover(permFds[i]);
        var alreadyIn = false;
        for (var j = 0; j < permResults.length; j++) {
            if (resultObjectsAreEqual(result.cov, permResults[j].cov)) {
                alreadyIn = true;
                break;
            }
        }
        if (!alreadyIn) {
            permResults.push(result);
        }
    }

    document.getElementById('outbis').innerHTML = permResults.length;

    var output = '';
    for (var i = 0; i < permResults[0].fds.length; i++) {
        output = output + attributePrint(permResults[0].fds[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(permResults[0].fds[i].right);
        output = output + '<br/>';
    }
    document.getElementById('out11b').innerHTML = output;

    canonicalCover(permResults[0].fds);

    calcDecomposition();
}

function getLeftOnly() {
    var attr = new Array();

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].left.length; j++) {
            if (attr.indexOf(fds[i].left[j]) == -1) {
                attr.push(fds[i].left[j]);
            }
        }
    }

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].right.length; j++) {
            if (attr.indexOf(fds[i].right[j]) != -1) {
                attr.splice(attr.indexOf(fds[i].right[j]), 1);
            }
        }
    }

    return uniq(attr);
}

function getLeftAndRight() {
    var attr = new Array();
    var attrLeft = new Array();
    var attrRight = new Array();

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].left.length; j++) {
            if (attrLeft.indexOf(fds[i].left[j]) == -1) {
                attrLeft.push(fds[i].left[j]);
            }
        }
    }

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].right.length; j++) {
            if (attrRight.indexOf(fds[i].right[j]) == -1) {
                attrRight.push(fds[i].right[j]);
            }
        }
    }

    for (var i = 0; i < attrLeft.length; i++) {
        if (attrRight.indexOf(attrLeft[i]) != -1 && attr.indexOf(attrLeft[i]) === -1) {
            attr.push(attrLeft[i]);
        }
    }

    for (var i = 0; i < attrRight.length; i++) {
        if (attrLeft.indexOf(attrRight[i]) != -1 && attr.indexOf(attrRight[i]) === -1) {
            attr.push(attrRight[i]);
        }
    }

    return uniq(attr);
}

function getRightOnly() {
    var attr = new Array();

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].right.length; j++) {
            if (attr.indexOf(fds[i].right[j]) == -1) {
                attr.push(fds[i].right[j]);
            }
        }
    }

    for (var i = 0; i < fds.length; i++) {
        for (var j = 0; j < fds[i].left.length; j++) {
            if (attr.indexOf(fds[i].left[j]) != -1) {
                attr.splice(attr.indexOf(fds[i].left[j]), 1);
            }
        }
    }

    return uniq(attr);
}

function getNoSide(l, b, r) {
    var attr = Array.from(relation);
    attr = diffArrays(attr, l);
    attr = diffArrays(attr, b);
    attr = diffArrays(attr, r);
    return uniq(attr);
}

/*****************************************
*              NORMALFORMEN              *
*****************************************/

function test2nf() {
    var output = '';

    var is2nf = true;

    // fuer jede rechte seite einer fd
    for (var i = 0; i < fds.length; i++) {
        // pruefe jedes b aus Beta
        for (var j = 0; j < fds[i].right.length; j++) {
            var partOfKey = false;
            // mit jedem kandidatenschluessel
            for (var k = 0; k < candidateKeys.length; k++) {
                // ob b teilmenge eines kandidatenschluessels
                if (isSubset(fds[i].right[j], candidateKeys[k])) {
                    partOfKey = true;
                    output = output + attributePrint(fds[i].right[j]) + ' aus der rechten Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist Teil des Kandidatenschlüssels ' + attributePrint(candidateKeys[k]) + '<br />';
                    break;
                }
            }
            // falls diese bedingung nicht erfuellt
            if (!partOfKey) {
                var countDep = 0;
                // teste fuer jeden kandidatenschluessel
                for (var k = 0; k < candidateKeys.length; k++) {
                    // ob b abhängig von einer echten teilmenge eines schluessels
                    if (isSubset(fds[i].left, candidateKeys[k]) && fds[i].left.length < candidateKeys[k].length) {
                        is2nf = false;
                        output = output + '<div class="text-danger">' + attributePrint(fds[i].right[j]) + ' aus der rechten Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist nicht Teil eines Kandidatenschlüssels und von einer echten Teilmenge des Kandidatenschlüssels ' + attributePrint(candidateKeys[k]) + ' abhängig<br /></div>';
                        break;
                    } else {
                        countDep++;
                    }
                }
                if (countDep == candidateKeys.length) {
                    output = output + attributePrint(fds[i].right[j]) + ' aus der rechten Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist nicht von einer echten Teilmenge eines Kandidatenschlüssels abhängig<br />';
                }           
            }
            if (!is2nf) {
                break;
            }
        }
        if (!is2nf) {
            break;
        }
    }
    document.getElementById("out10c").innerHTML = '1NF';
    // fuege zu export hinzu
    exportArray.push(['Normalformen']);
    exportArray.push(['1NF']);
    if (!is2nf) {
        output = output + '<div class="text-danger">Schema ist daher nicht in der zweiten Normalform</div>';
    } else {
        output = output + '<div class="text-success">Schema ist daher in der zweiten Normalform</div>';
        document.getElementById("out10c").innerHTML = document.getElementById("out10c").innerHTML + '<br>2NF';
        // fuege zu export hinzu
        exportArray.push(['2NF']);
    }
    document.getElementById("out9").innerHTML = output;
    if (is2nf) {
        test3nf();
    } else {
        document.getElementById("out10").innerHTML = '<p class="text-danger">Schema ist nicht in der zweiten Normalform und kann daher nicht in der dritten Normalform sein</p>';
        document.getElementById('out10b').innerHTML = '<p class="text-danger">Schema ist nicht in der dritten Normalform und kann daher nicht in der Boyce-Codd Normalform sein</p>';
    }
}

function test3nf() {
    var output = '';

    is3nf = true;

    //teste ob jede fd min. eine der drei bedingungen erfuellt
    for (var i = 0; i < fds.length; i++) {
        if (isTrivial(fds[i])) {
            output = output + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist trivial (' + attributePrint(fds[i].right) + ' &sube; ' + attributePrint(fds[i].left) +')<br />';
        } else if (isSuperKey(fds[i])) {
            output = output + 'Die linke Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist ein Superschlüssel<br/>';
        } else if (betaInCandidateKey(fds[i])) {
            output = output + 'Jedes Attribut der rechten Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist in einem Kandidatenschlüssel enthalten<br />';
        } else {
            is3nf = false;
            output = output + '<div class="text-danger">' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' entspricht keinem der Kriterien</div>';
            break;
        }
    }
    if (!is3nf) {
        output = output + '<div class="text-danger">Schema ist daher nicht in der dritten Normalform</div>';
        document.getElementById('out10b').innerHTML = '<p class="text-danger">Schema ist nicht in der dritten Normalform und kann daher nicht in der Boyce-Codd Normalform sein</p>';
    } else {
        output = output + '<div class="text-success">Schema ist daher in der dritten Normalform</div>';
        document.getElementById("out10c").innerHTML = document.getElementById("out10c").innerHTML + '<br/>3NF';
        // fuege zu export hinzu
        exportArray.push(['3NF']);
        testBCNF();
    }

    document.getElementById("out10").innerHTML = output;
}

function isTrivial(fd) {
    return isSubset(fd.right, fd.left);
}

function isSuperKey(fd) {
    // teste ob einer der kandidatenschluessel
    for (var i = 0; i < candidateKeys.length; i++) {
        // in der linken seite der fd beinhaltet ist
        if (isSubset(candidateKeys[i], fd.left)) {
            return true;
        }
    }
    return false;
}

function betaInCandidateKey(fd) {
    var x = 0;
    for (var i = 0; i < fd.right.length; i++) {
        for (var j = 0; j < candidateKeys.length; j++) {
            if (isSubset(fd.right[i], candidateKeys[j])) {
                x = x + 1;
                break;
            }
        }
    }

    return x == fd.right.length;
}

function testBCNF() {
    var output = '';

    isBCNF = true;

    for (var i = 0; i < fds.length; i++) {
        if (isTrivial(fds[i])) {
            output = output + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist trivial (' + attributePrint(fds[i].right) + ' &sube; ' + attributePrint(fds[i].left) +')<br />';
        } else if (isSuperKey(fds[i])) {
            output = output + 'Die linke Seite von ' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist ein Superschlüssel<br/>';
        } else {
            isBCNF = false;
            output = output + '<div class="text-danger">' + attributePrint(fds[i].left) + ' &rarr; ' + attributePrint(fds[i].right) + ' ist weder trivial, noch ein Superschlüssel<br /></div>'
            break;
        }
    }

    if (!isBCNF) {
        output = output + '<div class="text-danger">Schema ist daher nicht in der BCNF</div>';
    } else {
        output = output + '<div class="text-success">Schema ist daher in der BCNF</div>';
        document.getElementById("out10c").innerHTML = document.getElementById("out10c").innerHTML + '<br/>BCNF';
        // fuege zu export hinzu
        exportArray.push(['BCNF']);
    }

    document.getElementById("out10b").innerHTML = output;
}

/*****************************************
*         KANONISCHE UEBERDECKUNG        *
*****************************************/

function canonicalCover(fds, exportMe = false) {
    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['']);
    }

    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Kanonische Überdeckung']);
        exportArray.push(['Start FDs']);
    }

    var output = '';
    for (var i = 0; i < fds.length; i++) {
        output = output + attributePrint(fds[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(fds[i].right);
        output = output + '<br/>';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push([fds[i].left + ' -> ' + fds[i].right]);
        }
    }
    document.getElementById('out11b').innerHTML = output;

    var output = '';

    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Linksreduktion']);
    }

    var f = leftReduction(Array.from(fds), exportMe);

    for (var i = 0; i < f.length; i++) {
        output = output + attributePrint(f[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(f[i].right);
        output = output + '<br/>';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push([f[i].left + ' -> ' + f[i].right]);
        }
    }

    document.getElementById("out12").innerHTML = output;

    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Rechtsreduktion']);
    }

    f = rightReduction(f, exportMe);

    output = '';
    
    for (var i = 0; i < f.length; i++) {
        output = output + attributePrint(f[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(f[i].right);
        output = output + '<br/>';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push([f[i].left + ' -> ' + f[i].right]);
        }
    }

    document.getElementById("out14").innerHTML = output;

    f = removeEmptyRight(f);

    f = combineFds(f);

    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Zusammenfassung']);
    }

    output = '';
    
    for (var i = 0; i < f.length; i++) {
        output = output + attributePrint(f[i].left);
        output = output + ' &rarr; ';
        output = output + attributePrint(f[i].right);
        output = output + '<br/>';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push([f[i].left + ' -> ' + f[i].right]);
        }
    }

    document.getElementById("out15").innerHTML = output;

    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['']);
    }

    synthesis(f, exportMe);

    return f;
}

function nextCover() {
    cover++;
    if (cover > permResults.length) {
        cover = 1
    }
    canonicalCover(permResults[(cover - 1)].fds);
    document.getElementById('outvon').innerHTML = cover;
}

function prevCover() {
    cover--;
    if (cover == 0) {
        cover = permResults.length
    }
    canonicalCover(permResults[(cover - 1)].fds);
    document.getElementById('outvon').innerHTML = cover;
}

function leftReduction(f, exportMe) {
    var output = '';
    var fdsNew = new Array();
    // fuer jede fd
    for (var i = 0; i < f.length; i++) {
        // ersetze ggf alte durch neue fds
        for (var k = 0; k < fdsNew.length; k++) {
            f[k] = fdsNew[k];
        }
        var leftNew = Array.from(f[i].left);
        // fuer jedes A in alpha
        for (var j = 0; j < f[i].left.length; j++) {
            var alphaReduced = Array.from(leftNew);
            alphaReduced.splice(leftNew.indexOf(f[i].left[j]), 1);
            // wenn A aus alpha in hull(F, alpha - A)
            if (isSubset(f[i].left[j], calcAttrHull(f, alphaReduced))) {
                //  entferne A
                var a = attributePrint(f[i].left[j]);
                var b = attributePrint(alphaReduced);
                leftNew.splice(leftNew.indexOf(f[i].left[j]), 1);
                output = output + 'FD ' + (i + 1) + ': ' + a + ' entfällt, da &isin; AttrHülle(F, { ' + b + ' })<br />';
                if (exportMe) {
                    // fuege zu export hinzu
                    exportArray.push(['FD ' + (i + 1) + ': ' + f[i].left[j] + ' entfällt, da vorhanden in AttrHülle(F, { ' + alphaReduced + ' })']);
                }
            }
        }
        var fdNew = {left: new Array(), right: new Array()};
        fdNew.left = Array.from(leftNew);
        fdNew.right = Array.from(f[i].right);
        fdsNew.push(fdNew);
    }
    document.getElementById("out11").innerHTML = output;
    return fdsNew;
}

function rightReduction(f, exportMe) {
    var output = '';

    var fdsNew = new Array();
    // var modFds = Array.from(f);
    // fuer jede fd
    for (var i = 0; i < f.length; i++) {
        // ersetze ggf alte durch neue fds
        for (var k = 0; k < fdsNew.length; k++) {
            f[k] = fdsNew[k];
        }
        var rightNew = Array.from(f[i].right);
        // fuer jedes B in beta
        for (var j = 0; j < f[i].right.length; j++) {
            var betaReduced = Array.from(f[i].right);
            betaReduced.splice(f[i].right.indexOf(f[i].right[j]), 1);
            var fdReduced = {left: new Array(), right: new Array()};
            fdReduced.left = Array.from(f[i].left);
            fdReduced.right = Array.from(betaReduced);
            var modFds = Array.from(f);
            modFds.splice(f.indexOf(f[i]), 1);
            modFds.push(fdReduced);
            // wenn B aus beta in hull(F - (alpha -> beta) + (alpha -> (beta - B)), alpha)
            if (isSubset(f[i].right[j], calcAttrHull(modFds, f[i].left))) {
                //  entferne B
                var a = attributePrint(f[i].right[j]);
                var b = attributePrint(f[i].left);
                var c = attributePrint(f[i].right);
                rightNew.splice(rightNew.indexOf(f[i].right[j]), 1);
                output = output + 'FD ' + (i + 1) + ': ' + a + ' entfällt, da &isin; AttrHülle(F - ( ' + b + ' &rarr; ' + c + ' ), { ' + b + ' })<br />';
                if (exportMe) {
                    // fuege zu export hinzu
                    exportArray.push(['FD ' + (i + 1) + ': ' + f[i].right[j] + ' entfällt, da vorhanden in AttrHülle(F - ( ' + f[i].left + ' -> ' + f[i].right + ' ), { ' + f[i].left + ' })']);
                }
            }
        }
        var fdNew = {left: new Array(), right: new Array()};
        fdNew.left = Array.from(f[i].left);
        fdNew.right = Array.from(rightNew);
        fdsNew.push(fdNew);
    }
    document.getElementById('out13').innerHTML = output;
    return fdsNew;
}

function removeEmptyRight(f) {
    var fdsNew = new Array();

    // fuer jede fd
    for (var i = 0; i < f.length; i++) {
        // falls rechte seite nicht leer
        if (f[i].right.length > 0) {
            // uebernehme fd
            fdsNew.push(f[i]);
        }
    }

    return fdsNew;
}

function combineFds(f) {
    var fdsNew = new Array();

    // fuer jede fd
    for (var i = 0; i < f.length; i++) {
        var alreadyIn = false;
        // teste jede neu erstellte fd
        for (var j = 0; j < fdsNew.length; j++) {
            // auf gleichheit mit f[i] links
            if (isSubset(f[i].left, fdsNew[j].left) && isSubset(fdsNew[j].left, f[i].left)) {
                // fuege alle Betas hinzu
                for (var k = 0; k < f[i].right.length; k++) {
                    fdsNew[j].right.push(f[i].right[k]);
                }
                fdsNew[j].right = uniq(fdsNew[j].right);
                alreadyIn = true;
                break;
            }
        }
        // bzw ergaenze neue fds um f[i]
        if (!alreadyIn) {
            fdsNew.push(f[i]);
        }
    }

    return fdsNew;
}

/*****************************************
*           SYNTHESEALGORITHMUS          *
*****************************************/

function synthesis(can, exportMe = false) {
    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Synthesealgorithmus']);
    }
    if (!is3nf) {
        var schemes = new Array();
        // fuer jede fd aus kanon. ueberdeckung
        for (var i = 0; i < can.length; i++) {
            // bilde neues schema
            var s = {attributes: new Array(), fds: new Array()};
            for (var j = 0; j < can[i].left.length; j++) {
                s.attributes.push(can[i].left[j]);
            }
            for (var j = 0; j < can[i].right.length; j++) {
                s.attributes.push(can[i].right[j]);
            }
            s.attributes = uniq(s.attributes);
            s.fds.push(can[i]);
            schemes.push(s);
        }

        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Relationsschemata formen']);
        }

        var output = '';
        for (var i = 0; i < schemes.length; i++) {
            output = output + 'R' + (i + 1) + ' = { ' + attributePrint(schemes[i].attributes) + ' }<br/>';
            if (exportMe) {
                // fuege zu export hinzu
                exportArray.push(['R' + (i + 1) + ' = { ' + schemes[i].attributes + ' }']);
            }
        }
        document.getElementById('out16').innerHTML = output;

        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Schema mit Schlüssel hinzufügen']);
        }

        //pruefe ob ein schluessel in einer relation
        var keyInScheme = false;
        for (var i = 0; i < schemes.length; i++) {
            for (var j = 0; j < candidateKeys.length; j++) {
                if (isSubset(candidateKeys[j], schemes[i].attributes)) {
                    output = 'Der Kandidatenschlüssel ' + attributePrint(candidateKeys[j]) + ' ist bereits in R' + (i + 1) + ' enthalten.';
                    if (exportMe) {
                        // fuege zu export hinzu
                        exportArray.push(['Der Kandidatenschlüssel ' + candidateKeys[j] + ' ist bereits in R' + (i + 1) + ' enthalten.']);
                    }
                    keyInScheme = true;
                    break;
                }
            }
            if (keyInScheme) {
                break;
            }
        }
        // fuege hinzu wenn nicht
        if (!keyInScheme) {
            var s = {attributes: new Array(), fds: new Array()};
            for (var i = 0; i < candidateKeys[0].length; i++) {
                s.attributes.push(candidateKeys[0][i]);
            }
            schemes.push(s);
            var output = '';
            for (var i = 0; i < schemes.length; i++) {
                output = output + 'R' + (i + 1) + ' = { ' + attributePrint(schemes[i].attributes) + ' }<br/>';
                if (exportMe) {
                    // fuege zu export hinzu
                    exportArray.push(['R' + (i + 1) + ' = { ' + schemes[i].attributes + ' }']);
                }
            }
        }
        document.getElementById('out17').innerHTML = output;

        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Redundante Schemata entfernen']);
        }

        // entferne redundante
        var newSchemes = new Array();
        for (var i = 0; i < schemes.length; i++) {
            var schemesReduced = Array.from(schemes);
            schemesReduced.splice(i, 1);
            var alreadyIn = false;
            for (var j = 0; j < schemesReduced.length; j++) {
                // falls teilmenge
                if (isSubset(schemes[i].attributes, schemesReduced[j].attributes)) {
                    alreadyIn = true;
                    break;
                }
            }
            if (!alreadyIn) {
                newSchemes.push(schemes[i]);
            }
        }
        schemes = Array.from(newSchemes);

        output = '';
        for (var i = 0; i < schemes.length; i++) {
            output = output + 'R' + (i + 1) + ' = { ' + attributePrint(schemes[i].attributes) + ' }<br/>';
            if (exportMe) {
                // fuege zu export hinzu
                exportArray.push(['R' + (i + 1) + ' = { ' + schemes[i].attributes + ' }']);
            }
        }
        document.getElementById('out18').innerHTML = output;
        document.getElementById('out19b').innerHTML = output;

        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Schlüssel bestimmen']);
        }

        output = '';
        for (var i = 0; i < schemes.length; i++) {
            if (schemes[i].fds.length != 0) {
                output = output + 'Primäschlüssel für R' + (i + 1) + ' = { ' + attributePrint(schemes[i].fds[0].left) + ' }<br/>';
                if (exportMe) {
                    // fuege zu export hinzu
                    exportArray.push(['Primäschlüssel für R' + (i + 1) + ' = { ' + schemes[i].fds[0].left + ' }']);
                }
            } else {
                output = output + 'Primäschlüssel für R' + (i + 1) + ' = { ' + attributePrint(schemes[i].attributes) + ' }<br/>';
                if (exportMe) {
                    // fuege zu export hinzu
                    exportArray.push(['Primäschlüssel für R' + (i + 1) + ' = { ' + attributePrint(schemes[i].attributes) + ' }']);
                }
            }
        }
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['']);
        }
        document.getElementById('out19').innerHTML = output;
        document.getElementById('out19b').innerHTML = document.getElementById('out19b').innerHTML + '<br><h5>Schlüssel</h5>' + output;
        document.getElementById('button4').style.display = "inline";
    } else {
        document.getElementById('out19b').innerHTML = 'Schema ist bereits in 3NF';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Schema ist bereits in 3NF']);
            exportArray.push(['']);
        }
        document.getElementById('button4').style.display = "none";
        document.getElementById('button4').innerHTML='Rechenweg anzeigen';
        document.getElementById('synthesealgorithmusCollapse').classList.remove('show');
    }
}

/*****************************************
*           DEKOMPOSITION BCNF           *
*****************************************/

function calcDecomposition(exportMe = false) {
    if (exportMe) {
        // fuege zu export hinzu
        exportArray.push(['Dekompositionsalgorithmus']);
    }

    if (!isBCNF) {
        getExtendedFds();

        var r = {attributes: new Array(), fds: new Array(), name: ''};
        r.attributes = Array.from(relation);
        r.fds = Array.from(fdsPlusTrans);
        r.name = 'R';
        var schemes = new Array();
        schemes.push(r);

        var output = '';

        var notBCNF = true;

        while (notBCNF) {
            if (exportMe) {
                // fuege zu export hinzu
                exportArray.push(['Aufteilen der Relationen']);
            }
            notBCNF = false;
            var stop = false;
            // fuer jedes schema
            for (var i = 0; i < schemes.length; i++) {
                // teste jede fd
                for (var j = 0; j < schemes[i].fds.length; j++) {
                    // ob sie nicht in bcnf ist
                    if (!isTrivial(schemes[i].fds[j]) && !isSuperRel(schemes[i].fds, schemes[i].fds[j].left, schemes[i].attributes)) {
                        output = output + schemes[i].name + ' = { ' + attributePrint(schemes[i].attributes) + ' } ';
                        if (exportMe) {
                            // fuege zu export hinzu
                            exportArray.push([schemes[i].name + ' = { ' + schemes[i].attributes + ' } ']);
                        }
                        notBCNF = true;
                        // spalte auf
                        var newRi1 = {attributes: new Array(), fds: new Array(), name: ''};
                        var sum = Array.from(schemes[i].fds[j].left).concat(Array.from(schemes[i].fds[j].right));
                        newRi1.attributes = Array.from(uniq(sum));
                        newRi1.fds = findFds(newRi1.attributes);
                        newRi1.name = schemes[i].name + '1';
                        var newRi2 = {attributes: new Array(), fds: new Array(), name: ''};
                        newRi2.attributes = Array.from(schemes[i].attributes);
                        for (var k = 0; k < schemes[i].fds[j].right.length; k++) {
                            newRi2.attributes.splice(newRi2.attributes.indexOf(schemes[i].fds[j].right[k]), 1);
                        }
                        newRi2.fds = findFds(newRi2.attributes);
                        newRi2.name = schemes[i].name + '2';
                        output = output + 'wird aufgeteilt in ' + newRi1.name + ' = { ' + attributePrint(newRi1.attributes) + ' } und ' + newRi2.name + ' = { ' + attributePrint(newRi2.attributes) + ' }, denn ' + attributePrint(schemes[i].fds[j].left) + ' &rarr; ' + attributePrint(schemes[i].fds[j].right) + ' ist nicht in BCNF.<br/>';
                        if (exportMe) {
                            // fuege zu export hinzu
                            exportArray.push(['wird aufgeteilt in ' + newRi1.name + ' = { ' + newRi1.attributes + ' } und ' + newRi2.name + ' = { ' + newRi2.attributes + ' }']);
                        }
                        // ersetze
                        schemes[i] = newRi1;
                        schemes.splice((i + 1), 0, newRi2);
                        stop = true;
                        break;
                    }
                }
                if (stop) {
                    break;
                }
            }
        }
        document.getElementById('out20').innerHTML = output;

        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Ergebnis']);
        }

        output = '';
        for (var i = 0; i < schemes.length; i++) {
            output = output + schemes[i].name + ' = { ' + attributePrint(schemes[i].attributes) + ' }<br/>';
            if (exportMe) {
                // fuege zu export hinzu
                exportArray.push([schemes[i].name + ' = { ' + schemes[i].attributes + ' }']);
            }
        }

        document.getElementById('out21').innerHTML = output;
        document.getElementById('button5').style.display = "inline";
    } else {
        document.getElementById('out21').innerHTML = 'Schema ist bereits in BCNF';
        if (exportMe) {
            // fuege zu export hinzu
            exportArray.push(['Schema ist bereits in BCNF']);
        }
        document.getElementById('button5').style.display = "none";
        document.getElementById('button5').innerHTML='Rechenweg anzeigen';
        document.getElementById('dekompositionsalgorithmusCollapse').classList.remove('show');
    }

}

function getExtendedFds() {
    fdsPlusTrans = Array.from(fds);
    var extraFds = Array.from(fdHull());

    for (var i = 0; i < extraFds.length; i++) {
        fdsPlusTrans.push(extraFds[i]);
    }
}

function fdHull() {
    var fdsPlus = new Array();

    for (var i = 0; i < fds.length; i++) {
        var f = {left: Array.from(fds[i].left), right: Array.from(fds[i].right)};
        var newFd = {left: Array.from(fds[i].left), right: new Array()};
        var newRight = calcAttrHull(fds, Array.from(fds[i].left));
        newFd.right = Array.from(newRight);
        fdsPlus.push(newFd);
    }

    fdsPlus = combineFds(fdsPlus);

    return fdsPlus;
}

function findFds(attributes) {
    var returnArr = new Array();

    for (var i = 0; i < fdsPlusTrans.length; i++) {
        if (isSubset(fdsPlusTrans[i].left, attributes)) {
            var f = {left: Array.from(fdsPlusTrans[i].left), right: new Array()};
            for (var j = 0; j < fdsPlusTrans[i].right.length; j++) {
                if (isSubset(fdsPlusTrans[i].right[j], attributes) && !isSubset(fdsPlusTrans[i].right[j], f.left)) {
                    f.right.push(fdsPlusTrans[i].right[j]);
                }
            }
            if (f.right.length > 0) {
                if (!(isSubset(f.right, f.left) && isSubset(f.left, f.right))) {
                    returnArr.push(f);
                }
            }
        }
    }

    return returnArr;
}

function isSuperRel(f, k, attr) {
    return isSubset(attr, calcAttrHull(f, k));
}

/*****************************************
*             HILFSFUNKTIONEN            *
*****************************************/

function saveXlsx() {
    canonicalCover(permResults[(cover - 1)].fds, true);
    calcDecomposition(true);

    var wb = XLSX.utils.book_new();

    wb.Props = {
        Title: "DB Normalizer Export"
    };

    wb.SheetNames.push("Export");

    var ws_data = exportArray;

    var ws = XLSX.utils.aoa_to_sheet(ws_data);
    wb.Sheets["Export"] = ws;

    var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'export.xlsx');
}

function loadFile() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        showInfoModal('load');
    } else {
        showInfoModal('load-error');
    }
}

function readCells(workbook) {
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];

    var desired_cell = worksheet['A2'];
    var atts = (desired_cell ? desired_cell.v : undefined);

    document.getElementById('in1').value = atts;

    var row = 5;
    var readFds = new Array();
    while (true) {
        desired_cell = worksheet['A' + row];
        var fdVal = (desired_cell ? desired_cell.v : undefined);
        if (fdVal == undefined || fdVal.trim() ==  '') {
            break;
        }
        readFds.push(fdVal);
        row++;
    }
    document.getElementById('in2').value = readFds.join('\n');

    readIn1();
    readIn2();
    $('#infoModal').modal('hide');
}

function keyContained(allKeys, candidate) {
    for (var i = 0; i < allKeys.length; i++) {
        var sim = 0;
        for (var j = 0; j < candidate.length; j++) {
            if (allKeys[i].indexOf(candidate[j]) != -1) {
                sim++;
            }
        }
        if (sim === allKeys[i].length) {
            return true;
        }
    }
    return false;
}

function calcAttrHull(fds, attributes) {
    var result = Array.from(attributes);
    var change = true;

    while (change) {
        change = false;
        for (var i = 0; i < fds.length; i++) {
            if (fds[i].left.every(e => result.includes(e))) {
                for (var j = 0; j < fds[i].right.length; j++) {
                    if (result.indexOf(fds[i].right[j]) == -1) {
                        change = true;
                        result.push(fds[i].right[j]);
                    }
                }
            }
        }
    }

    return uniq(result);
}

function isSubset(a, b) {
    // falls a oder b kein array
    if (typeof a != 'object') {
        a = new Array(a);
    }

    if (typeof b != 'object') {
        b = new Array(b);
    }

    return a.every(e => b.includes(e));
}

function showInfoModal(topic) {
    if (topic == 'load') {
        var title = 'Datei auswählen';
        var content = '<input type="file" id="files" name="files[]" />';
    } else if (topic == 'load-error') {
        var title = 'Entschuldigung';
        var content = 'Dieser Browser unterstützt leider keine File APIs :(';
    } else if (topic === 'relation') {
        var title = 'Relation';
        var content = '<p><b>Bitte geben Sie die Attribute der Relation in das Textfeld ein.</b></p><p>Die einzelnen Attribute können durch folgende Zeichen getrennt werden:</p><ul><li><kbd class="bg-primary">Leerzeichen</kbd></li><li><kbd class="bg-primary">,</kbd> mit optional folgendem <kbd class="bg-primary">Leerzeichen</kbd></li></ul><p>Ist kein Trennzeichen vorhanden, werden die Attribute zeichenweise getrennt.<br/>Beispiel: <kbd class="bg-primary">ABCD</kbd> wird erfasst als: <kbd class="bg-primary">A</kbd> <kbd class="bg-primary">B</kbd> <kbd class="bg-primary">C</kbd> <kbd class="bg-primary">D</kbd></p>'
    } else if (topic === 'fd') {
        var title = 'Funktionale Abhängigkeiten';
        var content = '<p><b>Bitte geben Sie die Abhängigkeiten in das Textfeld ein.</b></p><p>Pro Zeile kann jeweils eine Abhängigkeit eingegeben werden.<br/>Die Seiten der Abhängigkeiten werden durch <kbd class=bg-primary>-></kbd> getrennt.<br/>Die Trennung der Attribute erfolgt gleich wie bei der Eingabe der Relation.</p><p><b>Hinweis</b><br/>Rot dargestellte <kbd class="bg-danger">Attribute</kbd> wurden nicht innerhalb der Attributseingabe erfasst.</p>';
    } else if (topic == 'schluessel') {
        var title = 'Schlüsselbestimmung';
        var content = '<p>Eine Attributmenge ist ein <b>Superschlüssel</b>, wenn ihre Attributhülle alle Attribute der Relation beinhaltet. Für jedes Attribut aus einem Superschlüssel kann überprüft werden, ob die Attributhülle noch immer alle Attribute der Relation beinhaltet, wenn das Attribut aus der Menge entfernt wurde. Sobald kein weiteres Attribut mehr entfernt werden kann handelt es sich bei der Attributmenge um einen <b>(Kandidaten-) Schlüssel</b>.</p><hr><p>Folgende Regeln können zur Schlüsselbestimmung eingesetzt werden:</p><p>Attribute die <b>nur links</b> auftauchen <b>müssen</b> im Schlüssel enthalten sein.</p><p>Attribute die <b>links und rechts</b> auftauchen <b>können</b> im Schlüssel enthalten sein.</p><p>Attribute die <b>nur rechts</b> auftauchen <b>können nicht</b> im Schlüssel enthalten sein.</p><hr><p><b>Hinweis:</b> Attribute die in keiner FD vorkommen nicht vergessen!</p>';
    } else if (topic == 'normalformen') {
        var title = 'Normalformen';
        var content = '<p>Die Relation ist in <b>1NF</b>, wenn alle Attribute <b>atomar</b> sind. Sie können also nicht weiter zerlegt werden. (Wir nehmen hier im Folgenden an, alle Attribute seien atomar.)</p><p>Die Relation ist in <b>2NF</b> wenn zudem für jedes Attribut ' + attributePrint(['b']) + ' auf der rechte Seite jeder FD gilt:<br>1. ' + attributePrint(['b']) + ' ist Teil eines Kandidatenschlüssels oder<br>2. ' + attributePrint(['b']) + ' ist nicht von einer echten Teilmenge eines Kandidatenschlüssels abhängig.</p><p>Das Schema ist in <b>3NF</b>, wenn für jede FD mindestens eine der folgenden Bedingungen erfüllt ist:<br>1. Die rechte Seite ist Teilmenge der linken Seite.<br>2. Die linke Seite ist ein Superschlüssel.<br>3. Jedes Attribut der rechten Seite ist in einem Kandidatenschlüssel enthalten.</p><p>Das Schema ist in <b>BCNF</b>, wenn für jede FD mindestens eine der folgenden Bedingungen erfüllt ist:<br>1. Die rechte Seite ist Teilmenge der linken Seite.<br>2. Die linke Seite ist ein Superschlüssel.</p>';
    } else if (topic == 'ueberdeckung') {
        var title = 'Kanonische Überdeckung';
        var content = '<p>Zuerst wird für jede vorhandene FD eine <b>Linksreduktion</b> durchgeführt. Dabei wird für jedes Attribut aus der linken Seite der FD überprüft, ob ihre rechte Seite auch noch dann in der Attributhülle der linken Seite beinhaltet ist, wenn das Attribut der linken Seite entfernt wurde. Falls dies zutrifft, wird die linke Seite der funktionalen Abhängigkeit jeweils um das Attribut reduziert. Dies wird für jede FD so oft wiederholt, bis keine Attribute mehr weggelassen werden können.</p><p>Mit den so entstandenen neuen FDs wird dann eine <b>Rechtsreduktion</b> durchgeführt. Dabei wird für jedes Attribut aus der rechten Seite jeder FD überprüft, ob dieses überflüssig ist. Hierfür wird die Attributhülle der linken Seite in Verbindung mit den FDs, welche um das entsprechende Attribut der FD reduziert (also (F - (&alpha; -> &beta;) &cup; (&alpha; -> (&beta; - B)))) wurden, berechnet. Sollte das betreffende Attribut Teil dieser Attributhülle sein, kann es aus der FD entfernt werden. Auch dieses Vorgehen wird so lange für alle Attribute aus allen FDs wiederholt, bis kein überflüssiges Attribut auf einer rechten Seite mehr vorhanden ist.</p><p>So ggf. entstandene FDs mit leerer rechter Seite werden anschließend <b>entfernt</b>.<p>Abschließend werden alle FDs mit der selben linken Seite zu einer FD <b>zusammengefasst</b>.</p></p>';
    } else if (topic == 'synthese') {
        var title = 'Synthesealgorithmus';
        var content = '<p>Das <b>Ergebnis der Berechnung der kanonischen Überdeckung</b> wird als Eingabe für den Synthesealgorithmus verwendet.</p><p>Aus jeder FD daraus wird ein <b>eigenes Relationenschema</b> gebildet. Dieses besitzt alle Attribute der jeweiligen Relation sowie alle FDs, die mit diesen Attributen aus der kanonischen Überdeckung gebildet werden können.</p><p>Falls keines der so erzeugten Schemata einen <b>Kandidatenschlüssel beinhaltet</b>, wird ein zusätzliches Relationenschema hinzugefügt, welches die Attribute eines beliebigen Schlüssels besitzt (und keine FDs).</p><p>Zuletzt werden ggf. entstandene überflüssige Relationen <b>entfernt</b>, d. h. ist ein Schema bereits in einem anderen Schema vollständig enthalten, wird das "kleinere" entfernt.</p>';
    } else if (topic == 'dekomposition') {
        var title = 'Dekompositionsalgorithmus';
        var content = '<p>Grundlage ist die <b>Menge Z</b>, bestehend aus der gegebenen <b>Relation R</b> als einziges darin befindliches Relationenschema.</p><p>Solange in Z ein Relationenschema Ri vorhanden ist, welches <b>nicht gänzlich in der BCNF</b> ist, wird dieses folgendermaßen behandelt:<br>Es wird eine der FDs daraus <b>gewählt</b>, die nicht der BCNF entspricht. Das gewählte Relationenschema Ri wird <b>zerlegt</b> in die Relationen Ri1 = &alpha; &cup; &beta; und Ri2 = Ri - &beta; (wobei sich &alpha; und &beta; jeweils auf die linke bzw. rechte Seite der gewählten Relation beziehen).<br>Anschließend wird Ri aus Z <b>entfernt</b>, Ri1 und Ri2 in Z <b>eingefügt</b> (also Z = (Z - {Ri}) &cup; {Ri1} &cup; {Ri2}).</p>';
    }
    document.getElementById('infoModalLabel').innerHTML = title;
    document.getElementById('infoModalText').innerHTML = content;
    $('#infoModal').modal('show');

    if (topic == 'load') {
        document.getElementById("files").addEventListener("change", handleFileSelect, false);
    }
}

function attributePrint(arr) {
    var output = '';

    if (arr.length === 0) {
        output = '<kbd class="bg-primary">KEINE</kbd>';
        return output;
    }

    for (l = 0; l < arr.length; l++) {
        if (relation.indexOf(arr[l]) != -1) {
            output = output + '<kbd class="bg-success">' + arr[l] + '</kbd> ';
        } else {
            output = output + '<kbd class="bg-danger">' + arr[l] + '</kbd> ';
        }
    }

    output = output.slice(0, -1);

    return output;
}

function prettyPrint(arr) {
    var out = '';
    for (var i = 0; i < arr.length; i++) {
        out = out + arr[i] + ', ';
    }
    out = out.slice(0, -2);
    return out;
}

function onClickLabelChange(event) {
    if(document.getElementById(event).innerHTML=='Rechenweg anzeigen'){
        document.getElementById(event).innerHTML='Rechenweg verbergen';
    }
    else{
        document.getElementById(event).innerHTML='Rechenweg anzeigen';
    }
}

function loadPreSet(number) {
    var preSetAttributes = [ ['A', 'B', 'C', 'D', 'E', 'F'], ['A', 'B', 'C', 'D', 'E', 'F'], ['person', 'kindName', 'kindAlter', 'fahrradTyp', 'fahrradFarbe'], ['A', 'B', 'C', 'D', 'E', 'F'], ['A', 'B', 'C', 'D'] ];
    var preSetFds = [ ['B->DA', 'DEF->B', 'C->EA'], ['A->BC', 'C->DA', 'E->ABC', 'F->CD', 'CD->BEF'], ['person->kindName,kindAlter', 'person->fahrradTyp,fahrradFarbe', 'kindName->kindAlter'], ['AB->CD', 'ABC->D', 'E->C', 'D->C', 'CDE->AB'], ['AB->C', 'BC->D', 'BA->CD', 'DA->B'] ];

    document.getElementById('in1').value = prettyPrint(preSetAttributes[number]);
    document.getElementById('in2').value = preSetFds[number].join('\n');
}

function resultObjectsAreEqual(a, b) {
    var bCopy = Array.from(b);
    if (a.length == bCopy.length) {
        for (var i = 0; i < a.length; i++) {
            var foundFd = false;
            for (var j = 0; j < bCopy.length; j++) {
                if (isSubset(a[i].left, bCopy[j].left) && isSubset(bCopy[j].left, a[i].left) && isSubset(a[i].right, bCopy[j].right) && isSubset(bCopy[j].right, a[i].right)) {
                    foundFd = true;
                    bCopy.splice(j, 1);
                    break;
                }
            }
            if (!foundFd) {
                return false;
            }
        }
        return true;
    }
    return false;
}

/*****************************************
*          3RD-PARTY FUNKTIONEN          *
*****************************************/

// https://stackoverflow.com/a/9229821
function uniq(a) {
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    })
}

// https://stackoverflow.com/a/39089422
function getSubArrays(arr){
    if (arr.length === 1) return [arr];
    else {
        subarr = getSubArrays(arr.slice(1));
        return subarr.concat(subarr.map(e => e.concat(arr[0])), [[arr[0]]]);
    }
}

// https://stackoverflow.com/a/1723220
function diffArrays(arr, arr2) {
    return arr.filter(function(x) { return arr2.indexOf(x) < 0 })
}

// https://stackoverflow.com/a/43260158/
function perm(xs) {
    let ret = [];
    for (let i = 0; i < xs.length; i = i + 1) {
        let rest = perm(xs.slice(0, i).concat(xs.slice(i + 1)));
        if(!rest.length) {
            ret.push([xs[i]])
        } else {
            for(let j = 0; j < rest.length; j = j + 1) {
                ret.push([xs[i]].concat(rest[j]))
            }
        }
    }
    return ret;
}

// https://redstapler.co/sheetjs-tutorial-create-xlsx/
function s2ab(s) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf);  //create uint8array as viewer
    for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
    return buf;
}

// https://github.com/sheetjs/js-xlsx
function handleFileSelect(e) {
    var files = e.target.files, f = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
        var data = new Uint8Array(e.target.result);
        var workbook = XLSX.read(data, {type: 'array'});
        readCells(workbook);
    };
    reader.readAsArrayBuffer(f);
}
